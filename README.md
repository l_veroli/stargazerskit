**Stargazers List**

Il progetto permette la visualizzazione della lista degli stargazers di un repository, ovvero la lista degli utenti che lo hanno inserito tra i preferiti. 
Inserito il nome del repository e dell'owner, viene mostrata una lista di utenti contenente avatar e username. 

---

- Il progetto è stato sviluppato utilizzando il linguaggio **Kotlin**.
- L'applicazione gestisce la navigazione dei fragment utilizzando il **Navigation Component** di Android (definendo quindi un *nav_graph* contenente tutte le varie destinazioni). 
- Il caricamento dell'icona dell'avatar di ogni utente è stato gestito utilizzando la libreria **Glide**. 
- Lo strato di rete è basato su **Retrofit** e convertitore **Gson**, utilizzando le **Coroutines** come strumento di chiamata.

---

Una demo dell'applicazione può essere trovata al seguente link: https://drive.google.com/open?id=1h_cClzv8yc3YDmY73gAhG8CPxlSJOckY