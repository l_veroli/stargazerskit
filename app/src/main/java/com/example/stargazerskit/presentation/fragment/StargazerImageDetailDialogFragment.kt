package com.example.stargazerskit.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.example.stargazerskit.R
import kotlinx.android.synthetic.main.layout_user_image_detail_dialog_fragment.*

class StargazerImageDetailDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_user_image_detail_dialog_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { args ->
            val imageUrl = StargazerImageDetailDialogFragmentArgs.fromBundle(args).imageUrl
            val userName = StargazerImageDetailDialogFragmentArgs.fromBundle(args).stargazerUsername

            context?.let {
                Glide.with(it)
                    .load(imageUrl)
                    .placeholder(R.drawable.avatar_placeholder)
                    .circleCrop()
                    .into(stargazerIcon)

                stargazerUsername.text = userName
            }
        }
    }
}