package com.example.stargazerskit.presentation.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.stargazerskit.R
import com.example.stargazerskit.management.StargazersScrollListener
import com.example.stargazerskit.management.model.Stargazer
import com.example.stargazerskit.presentation.adapter.StargazersAdapter
import com.example.stargazerskit.presentation.contract.StargazersListContract
import com.example.stargazerskit.presentation.presenter.StargazersListPresenter
import kotlinx.android.synthetic.main.layout_stargazers_list_fragment.*

class StargazersListFragment : Fragment(), StargazersListContract.MainView,
    StargazersAdapter.AdapterInteractionListener {

    private val MAX_STARGAZER_NUMBER_SMOOTH_SCROLL = 20
    private var mInteractionListener: StargazersListInterface? = null
    private var mPresenter: StargazersListPresenter? = null
    private var mPageNumber: Int = 1
    private var mEndOfList: Boolean = false
    private var mIsLoading: Boolean = false
    private var mRepository: String? = null
    private var mRepositoryOwner: String? = null
    private lateinit var mAdapter: StargazersAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is StargazersListInterface) {
            mInteractionListener = context
        }
    }

    override fun onDetach() {
        mInteractionListener = null
        super.onDetach()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_stargazers_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mInteractionListener?.refreshToolbar(getString(R.string.repository), true)
        arguments?.let { args ->
            mRepository = StargazersListFragmentArgs.fromBundle(args).repositoryName
            mRepositoryOwner = StargazersListFragmentArgs.fromBundle(args).repositoryOwner

            recyclerView.layoutManager = LinearLayoutManager(context)
            (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            mAdapter = StargazersAdapter(context, 5, this)
            recyclerView.adapter = mAdapter

            scrollUpButton.setOnClickListener {
                val firstVisible =
                    (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                if (firstVisible < MAX_STARGAZER_NUMBER_SMOOTH_SCROLL) {
                    recyclerView.smoothScrollToPosition(0)
                } else {
                    recyclerView.scrollToPosition(0)
                }
            }

            mPresenter = StargazersListPresenter(this)
            mPresenter?.getRepositoryStargazersList(
                mRepository,
                mRepositoryOwner,
                mPageNumber
            )
        } ?: run {
            AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.attention))
                .setMessage(getString(R.string.section_not_allowed))
                .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                    activity?.onBackPressed()
                }
                .show()
        }
    }


    private fun manageRecylcerViewScroll() {
        recyclerView.addOnScrollListener(object :
            StargazersScrollListener(recyclerView.layoutManager as LinearLayoutManager) {
            override fun loadMoreStargazers() {
                mPageNumber++
                mIsLoading = true
                mPresenter?.getRepositoryStargazersList(mRepository, mRepositoryOwner, mPageNumber)
            }

            override fun isLastPage(): Boolean {
                return mEndOfList
            }

            override fun isLoading(): Boolean {
                return mIsLoading
            }

            override fun showScrollUpButton() {
                scrollUpButton.visibility = View.VISIBLE
            }

            override fun hideScrollUpButton() {
                scrollUpButton.visibility = View.GONE
            }
        })
    }

    override fun onFirstStargazersListRetrieved(repoList: ArrayList<Stargazer>) {
        if (repoList.isEmpty()) {
            mAdapter.addErrorMessage(getString(R.string.no_repository_or_stargazers_found))
        } else {
            mAdapter.setData(repoList)
            mIsLoading = false

            manageRecylcerViewScroll()
        }
    }

    override fun onNextStargazersPageRetrieved(stargazersList: ArrayList<Stargazer>) {
        if (stargazersList.isEmpty()) {
            mEndOfList = true
            mAdapter.removeLoaderElement()
            mIsLoading = false
        } else {
            mAdapter.setData(stargazersList)
            mIsLoading = false
        }
    }

    override fun onRetrievingStargazersListError() {
        mAdapter.addErrorMessage(getString(R.string.no_repository_or_stargazers_found))
    }

    override fun onPhoneNotConnected() {
        mAdapter.addErrorMessage(getString(R.string.network_unreachability_message))
    }

    override fun onStargazerClicked(stargazer: Stargazer) {
        val direction = StargazersListFragmentDirections.openStargazerImageDetail(
            stargazer.avatarUrl,
            stargazer.username
        )
        mInteractionListener?.openStargazerDetail(direction)
    }

    interface StargazersListInterface {
        fun refreshToolbar(title: String, shouldShowBackButton: Boolean)
        fun openStargazerDetail(direction: NavDirections)
    }
}