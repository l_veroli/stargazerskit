package com.example.stargazerskit.presentation.contract

import android.content.Context
import com.example.stargazerskit.management.model.Stargazer

interface StargazersListContract {


    interface MainView {
        fun getContext(): Context?

        fun onFirstStargazersListRetrieved(repoList: ArrayList<Stargazer>)
        fun onNextStargazersPageRetrieved(stargazersList: ArrayList<Stargazer>)
        fun onRetrievingStargazersListError()

        fun onPhoneNotConnected()
    }

    interface MainPresenter {
        fun getRepositoryStargazersList(repository: String?, repositoryOwner: String?, pageNumber: Int)
        fun cancelJob()
    }
}