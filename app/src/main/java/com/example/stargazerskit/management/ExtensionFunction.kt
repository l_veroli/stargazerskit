package com.example.stargazerskit.management

fun String?.isNotNullAndNotEmpty(): Boolean{
    return this != null && this.isNotEmpty()
}