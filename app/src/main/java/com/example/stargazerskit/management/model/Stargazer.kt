package com.example.stargazerskit.management.model


data class Stargazer(val username: String,
                     val avatarUrl: String)