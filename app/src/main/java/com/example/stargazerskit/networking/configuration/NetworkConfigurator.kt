package com.example.stargazerskit.networking.configuration

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkConfigurator {

    private val CONNECTION_TIMEOUT = 10L

    private fun buildOkHttpClient(): OkHttpClient {
        val logInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(logInterceptor)
            .build()
    }


    private fun buildGsonConverter(): GsonConverterFactory{
        return GsonConverterFactory.create(
            GsonBuilder()
                .setLenient()
                .disableHtmlEscaping()
                .create()
        )
    }


    val properOkHttpClient: OkHttpClient by lazy {
        buildOkHttpClient()
    }
    val properGsonConverter: GsonConverterFactory by lazy {
        buildGsonConverter()
    }

}