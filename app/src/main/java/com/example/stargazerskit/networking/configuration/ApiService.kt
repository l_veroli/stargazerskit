package com.example.stargazerskit.networking.configuration

import com.example.stargazerskit.networking.model.NetworkStargazer
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {


    @Headers("Content-Type: application/json")
    @GET(NetworkUrl.REPO_STARGAZERS_LIST)
    fun retrieveRepoStargazersListAsync(
        @Path("owner") owner: String,
        @Path("repo") repository: String,
        @Query("page") pageNumber: Int

    ): Deferred<Response<ArrayList<NetworkStargazer>>>
}