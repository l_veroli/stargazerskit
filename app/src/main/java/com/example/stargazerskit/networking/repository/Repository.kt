package com.example.stargazerskit.networking.repository

import com.example.stargazerskit.networking.configuration.ApiService
import com.example.stargazerskit.networking.configuration.Injection
import com.example.stargazerskit.networking.model.NetworkStargazer
import timber.log.Timber
object Repository {

    private const val TAG = "StargazersKitRepository"

    suspend fun retrieveRepositoryStargazerList(repository: String, repositoryOwner: String, pageNumber: Int): ArrayList<NetworkStargazer>?{
        val deferred = Injection.properNetworkClient
            .create(ApiService::class.java)
            .retrieveRepoStargazersListAsync(repositoryOwner, repository, pageNumber)

        return try {
            val response = deferred.await()
            if(response.isSuccessful){
                response.body()
            }else{
                null
            }
        }catch (exception: Throwable){
            Timber.tag(TAG).e("retrieveRepositoryStargazerList - an error occurs within the network call")
            null
        }
    }
}