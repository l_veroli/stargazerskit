package com.example.stargazerskit.networking.configuration

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit

object Injection {

    private fun buildRetrofitClient(): Retrofit{
        return Retrofit.Builder()
            .baseUrl(NetworkUrl.BASE_URL)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(NetworkConfigurator.properGsonConverter)
            .client(NetworkConfigurator.properOkHttpClient)
            .build()
    }

    val properNetworkClient: Retrofit by lazy {
        buildRetrofitClient()
    }
}