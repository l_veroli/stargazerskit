package com.example.stargazerskit.networking.configuration

object NetworkUrl {

    const val BASE_URL = "https://api.github.com"
    const val REPO_STARGAZERS_LIST = "/repos/{owner}/{repo}/stargazers"
}