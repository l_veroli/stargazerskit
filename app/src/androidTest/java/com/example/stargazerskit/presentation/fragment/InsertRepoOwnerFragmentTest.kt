package com.example.stargazerskit.presentation.fragment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.example.stargazerskit.R
import com.example.stargazerskit.presentation.activity.StargazersMainActivity
import org.junit.Rule
import org.junit.Test
import java.lang.Exception


class InsertRepoOwnerFragmentTest{

    @Rule @JvmField
    var activity= ActivityTestRule<StargazersMainActivity>(StargazersMainActivity::class.java, true, true)

    @Test
    @Throws(Exception::class)
    fun searchButtonTest_checkNotEmptyField(){
        onView(withId(R.id.repoEditText)).perform(ViewActions.typeText(""))
        onView(withId(R.id.ownerEditText)).perform(ViewActions.typeText(""))
        onView(withId(R.id.searchButton)).perform(ViewActions.click())
    }
}